/*
This program is a PoC trying to measure bandwidth (B/W) for SPICE protocol.
It does not use SPICE protocol itself but a simplified version using PING/PONG
messages.

The simplified protocol is very simple:
- bytes 0xff from server are like PING messages (requires a PONG)
- any other bytes are ignored (other traffic)
- when client receives a PING it sends a PONG (a simple 0xff byte)

So the program:
- handle multiple TCP connections
- the client listen (on SPICE is the opposite but it does not matter),
  receive bytes and send back PONGs on PINGs
- the server connects and send data (with PING inside)
- the server on PONGs update statistics on channel (every TCP connection
  represents a channel) and totals
*/
package main

import (
	"fmt"
	"math/rand"
	"net"
	"time"
	"io"
	"sync"
)

var is_theory bool
var theory_now int

func now() time.Time {
	if is_theory {
		return time.Unix(int64(theory_now), 0)
	} else {
		return time.Now()
	}
}

func set_now(t int) {
	if !is_theory {
		panic("called set_now not theory")
	}
	theory_now = t
}

var host = "192.168.127.1"
var wg sync.WaitGroup

type ConnInfo struct {
	used bool
	start_time time.Time
	last_time time.Time
	partial_time float64
	last_completed_partial_time float64
	total_bytes uint
}
var conns [10]ConnInfo
var num_conn int
var mtx sync.Mutex

var partial_time float64 // completed time
var total_bytes uint // completed bytes

var start_time time.Time // Use only for debugging
var rtt int64

func alloc_info() int {
	mtx.Lock()
	defer mtx.Unlock()

	now := now().Add(time.Duration(rtt))
	if num_conn == 0 {
		start_time = now
	}
	for idx := range conns {
		c := &conns[idx]
		if !c.used {
			continue
		}
		sec := now.Sub(c.last_time).Seconds()
		c.last_time = now
		c.partial_time += sec / float64(num_conn)
	}
	fmt.Println("VVVVVVVVVVVVVVVVVVVV")
	fmt.Println("Allocating")
	print_info()
	fmt.Println("^^^^^^^^^^^^^^^^^^^^")
	for idx := range conns {
		c := &conns[idx]
		if c.used {
			continue
		}
		c.start_time = now
		c.last_time = now
		c.used = true
		c.partial_time = 0
		c.last_completed_partial_time = 0
		c.total_bytes = 0
		num_conn++
		return idx
	}
	panic("too much connections")
}

func free_info(i int) {
	mtx.Lock()
	defer mtx.Unlock()

	if !conns[i].used {
		panic("freeing twice")
	}
	conns[i].used = false
	// we need to update timings of other ones
	now := now()
	for idx := range conns {
		c := &conns[idx]
		if !c.used {
			continue
		}
		sec := now.Sub(c.last_time).Seconds()
		c.last_time = now
		c.partial_time += sec / float64(num_conn)
	}
	num_conn--
}

func print_info() {
	for idx := range conns {
		c := &conns[idx]
		if !c.used {
			continue
		}
		fmt.Println(idx)
		fmt.Println("   start_time", c.start_time.Sub(start_time).Seconds())
		fmt.Println("   last_time", c.last_time.Sub(start_time).Seconds())
		fmt.Println("   partial_time", c.partial_time)
		fmt.Println("   total_bytes", c.total_bytes)
	}
}

func update_info(i int, bytes uint) float64 {
	mtx.Lock()
	defer mtx.Unlock()

	if !conns[i].used { panic("indalid connection id") }
	fmt.Println("VVVVVVVVVVVVVVVVVVVV")
	now := now()
	fmt.Println("updating", i, "bytes", bytes, "now", now.Sub(start_time).Seconds(), num_conn)

	c := &conns[i]

	// NOTE these 3 lines are quite a pattern, make a function
	sec := now.Sub(c.last_time).Seconds()
	c.last_time = now
	c.partial_time += sec / float64(num_conn)

	c.total_bytes += bytes
	partial_time += c.partial_time - c.last_completed_partial_time
	total_bytes += bytes
	c.last_completed_partial_time = c.partial_time

	print_info()
	fmt.Println("Total", total_bytes, "time", partial_time)
	fmt.Println("^^^^^^^^^^^^^^^^^^^^")

	speed := float64(c.total_bytes) / c.partial_time
	fmt.Println("======>", i, "Average partial", speed)
	speed2 := float64(total_bytes) / partial_time
	fmt.Println("Average", speed2)
	if is_theory && speed != speed2 { panic("ciao") }

	return speed
}

func reset_info() {
	for idx := range conns {
		c := &conns[idx]
		c.used = false
	}
	num_conn = 0
	partial_time = 0
	total_bytes = 0
}

func rand_bytes(size uint) []byte {
	buf := make([]byte, size)
	rand.Read(buf)
	for i, b := range buf {
		buf[i] = b & 0x7f
	}
	return buf
}

func main() {

	theory()
	is_theory = false
	reset_info()

//	rtt = 40 /* RTT in ms */ * 1000000

	server, err := net.Listen("tcp", ":1234")
	if server == nil {
		panic("couldn't start listening: " + err.Error())
	}
	go pong_part(server)

	wg.Add(2)
	go test([]uint{10000, 10000, 100000, 1000000})
	go test([]uint{20000, 40000, 500000})

	wg.Wait()
}

/* Handle data from test */
func pong_part(listener net.Listener) {
	for {
		client, err := listener.Accept()
		if client == nil {
			fmt.Printf("couldn't accept: " + err.Error())
			continue
		}
		go simple_replier(client)
        }
}

/* Receive bytes from test and send back "PONG" */
func simple_replier(client net.Conn) {
	buf := make([]byte, 32 * 1024)
	defer client.Close();
	tot := 0
	for {
		l, err := client.Read(buf)
		if err != nil {
			break
		}
		for i, b := range buf[:l] {
			if b == 0xff {
				fmt.Println("Got ping request @", i + tot)
				// send PONG
				client.Write([]byte{0xff})
			}
		}
		tot += l
	}
	fmt.Println("exit client")
}

func test(sizes []uint) {
	defer wg.Done()

	info := alloc_info()
	defer free_info(info)

	start_time := now()

	var sum uint = 0
	for _, s := range sizes {
		sum += s
	}
	b := rand_bytes(sum)
	sum = 0
	for _, s := range sizes {
		sum += s
		b[sum - 1] = 0xff
	}
	conn, err := net.Dial("tcp", host + ":1234")
	if conn == nil {
		panic("couldn't start listening: " + err.Error())
	}
	defer conn.Close()
	go conn.Write(b)
	// wait for PONGs
	sum = 0
	pong := make([]byte, 1)
	for _, s := range sizes {
		sum += s
		io.ReadFull(conn, pong)
		sec := time.Since(start_time).Seconds()
		update_info(info, s)
		fmt.Println("Got pong", pong, "size", sum, float64(sum) / sec * 8)
	}
}

func theory() {
/*
The algorithm tries to estimate the bandwidth we have.
Note that we measure the bandwidth we can use, we don't care the bandwidth
of the connection with the next hop but the bandwidth we can use with the
other end.
Initially I didn't consider the delay between the 2 ends, this should be
taken into account considering RTT time. Note that using SPICE ping/pong
messages for measuring the RTT will contain user-space delays.
No delays also means that the speed changes instantaneously, this is not
true for real connections, it will take some time for the speed to increase.
I assume that the multiple connections with the other end will share evenly
the B/W. Although this is true unless you have QoS setting in the OS and
in long data transmission this is not 100% correct in real cases. This is
due to the queueing. Although the kernel try to be more fair as possible
there's always queue handling, if a connection try to send 1MB (for instance)
the kernel will queue some packets in order to take advantage of NIC
availability. Even if we send data ASAP from another connection the queue
won't be entirely rewritten so the first connection will be slightly faster
at the beginning. This is less significant if data take more time to be sent.
*/
	var info, info2, info3 int
	var speed float64
	is_theory = true
/*
    \Time 0          1
Flow \    0123456789 0123456789
A         *******
B            ######

At time 7 I receive first pong (A), now
- I sent * for 3 slot and * and # for 4 slots
- 3 slots at 100% speed
- 4 slots at 50% speed
- so total = 3 * B/W + 4 * 0.5 * B/W -> total = (3 + 2) * B/W ->
  -> B/W = total / (3 + 2)

At time 9 I receive second pong (B), now
1- I take into account all
  - I sent total = total A + total B
  - time is 9
  - B/W = total / 9
2- I take into account only B
  - I sent total = total B
  - I sent * and # for 4 slots and # for 2 slots
  - 4 slots at 50% speed
  - 2 slots at 100% speed
  - so total = 4 * 0.5 * B/W + 2 * B/W -> total = (2 + 2) * B/W ->
    -> B/W = total / (2 + 2)
*/
	set_now(0)
	info = alloc_info()
	set_now(3)
	info2 = alloc_info()
	set_now(7)
	speed = update_info(info, 50)
	free_info(info)
	// now speed should be 10
	if speed != 10 { panic("speed should be 10") }
	set_now(9)
	speed = update_info(info2, 40)
	if speed != 10 { panic("speed should be 10") }

/*
    \Time 0          1
Flow \    0123456789 0123456789
A         **********
B            ####

At time 10 I receive second pong (A), now
1- I take into account only A
  - I sent * for 10 slots and # for 4 slots
  - 4 slots at 50%
  - 6 slots at 100%
  - so total A = 4 * 0.5 * B/W + 6 * B/W -> total A = (2 + 6) * B/W ->
    -> B/W = total A / (2 + 6)
2- I take into account all
  - I sent total = total A + total B
  - B/W = total / 10
  - total B = total A * (4 * 0.5) / (4 * 0.5 + 6) ->
    -> total B = total A / 4
  - -> B/W = (total A + total A / 4) / 10 ->
    -> B/W = total A * 5 / 4 / 10 ->
    -> B/W = total A / 8 (same as case 1)
*/
	reset_info()
	set_now(0)
	info = alloc_info()
	set_now(3)
	info2 = alloc_info()
	set_now(7)
	speed = update_info(info2, 20)
	free_info(info2)
	if speed != 10 { panic("speed should be 10") }
	set_now(10)
	speed = update_info(info, 80)
	if speed != 10 { panic("speed should be 10") }

/*
    \Time 0          1
Flow \    0123456789 0123456789
A         *********
B           ########
C              $$$

At time 9 I receive second pong (A), now
- I send * for 9 slots
- 2 slots at 100%
- 4 slots at 50%
- 3 slots at 33.3%
I could save a list of (time, # sending) and then use it to compute the relative time.
Or update the timing of each channel with
- last time event
- last time count
- relative time before last time event
Also we could keep
- total bytes from first time count == 1
So then count get to 0 we could compute total in this long range
*/
	reset_info()
	set_now(0)
	info = alloc_info()
	set_now(2)
	info2 = alloc_info()
	set_now(5)
	info3 = alloc_info()
	set_now(8)
	speed = update_info(info3, 10)
	if speed != 10 { panic("speed should be 10") }
	free_info(info3)
	set_now(9)
	speed = update_info(info, 50)
	if speed != 10 { panic("speed should be 10") }
	free_info(info)

/*
    \Time 0          1
Flow \    0123456789 0123456789
A         ********
B            #######
C              $$$

At time 7 I receive first pong (A), now
1- I want to consider all flows
  - I sent * for 3 slots at 100%, 2 slots at 50%, 3 slots at 33.3%
  - I sent # for 3 slots at 33.3%, 2 slots at 50%
  - I sent $ for 3 slots at 33.3$
  - total completed send = total A + total C (total B is not completed)
  - I account time for the totals I have so 3 + 2 * 0.5 + 3 * 1/3 + 3 * 1/3 = 6
  - B/W = (total A + total B) / 6
*/
	reset_info()
	set_now(0)
	info = alloc_info()
	set_now(3)
	info2 = alloc_info()
	set_now(5)
	info3 = alloc_info()
	set_now(8)
	speed = update_info(info3, 10)
	free_info(info3)
	speed = update_info(info, 50)
	if speed != 10 { panic("speed should be 10") }
}
